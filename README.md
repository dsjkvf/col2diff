# col2diff

## About

**col2diff** is a small [AWK](https://en.wikipedia.org/wiki/AWK) script that will compare two separator-delimited (default is space or tab) data files one against another according to the specified column (default is 1).

## Usage

  - Compare data1.txt and data2.txt by the 1 column:
    - `col2diff data1.txt data2.txt`
  - Compare data3.txt and data4.txt by the 3 column:
    - `col2diff data3.txt data4.txt -col 3`
  - Compare data5.txt and data6.txt by the 2 column and with the delimiter set to `;`:
    - `col2diff data3.txt data4.txt -sep ";" -col 3`

### Examples

Let's say we have the following files:

  - MyData.txt:

````
    123 foo1 bar 777
    456 foo2 aoo 666
    789 foo3 raa 999
````

  - YourData.txt:

````
    123    abc    777    bar
    213    cba    666    foo
    321    bac    777    boo
    312    bca    777    roo
````

Then, running `col2diff MyData.txt YourData.txt` will produce the following:

    < 456    foo2 aoo 666
    < 789    foo3 raa 999
    
    > 321    bac 777 boo
    > 312    bca 777 roo
    > 213    cba 666 foo

Also, running `col2diff MyData.txt YourData.txt -col 2` will obviously result like this:

    < foo1    123 bar 777
    < foo2    456 aoo 666
    < foo3    789 raa 999
    
    > abc    123 777 bar
    > cba    213 666 foo
    > bca    312 777 roo
    > bac    321 777 boo

And besides that, running `col2diff MyData.txt YourData.txt -col 3` will fail because of the repeated indices present in the 3rd column.

### Employment

Now, what can it be useful for? For example, if you need to checksum compare two different folders. First of all, it can be done manually:

  - Write down MD5 checksums for the contents of the folders in question:
    - `md5deep -b /Folder/This/* > dir1.txt`
    - `md5deep -b /Folder/That/* > dir2.txt`
  - And then run the provided script against the collected data like this:
    - `col2diff dir1.txt dir2.txt`
  - Voilà!

Or, you may simply use the included `dir-diff-md5` wrapper:

    dir-diff-md5 /Folder/This /Folder/That
